var cols, rows;
var w = 20;
var grid = [];
var current;
var stack = [];
function setup() {
  createCanvas(800,600);
  cols = Math.floor(width / w);
  rows = Math.floor(height / w);
  
  for(var j = 0; j < rows; j++){
    for(var i = 0; i < cols; i++){
      var cell = new Cell(i, j)
      grid.push(cell);

    }
  }
  current = grid[0];
  //console.log(current)
}

function draw() {
  frameRate(30);
  background(51);
  
  for(var i = 0; i < grid.length; i++){
    grid[i].show();
  }
  current.visited = true;
  var next = current.checkNeighbours();
  //console.log(next)
  if(next){
    //STEP 2 - creating stack
    stack.push(current);
    
    //STEP 3 - romoving borders
    var borderCleanCurrent = [next.i - current.i, next.j - current.j]
    var borderCleanNext = [current.i - next.i, current.j - next.j]
    // top: [0, -1], right: [1, 0], bottom: [0, 1], left: [-1, 0]
    current.removeWalls(borderCleanCurrent);
    next.removeWalls(borderCleanNext);
    //STEP 4
    next.visited = true;
    //console.log(nowRemoving(borderCleanCurrent), nowRemoving(borderCleanNext))
    current.current = false;
    current = next;
    current.current = true;
  }else if (stack.length > 0){
    current.current = false;
    current = stack.pop();
    current.current = true;
  }
  
}
function nowRemoving(tab){
  if(tab[1] === -1){//top
      return "top"
    }else if(tab[0] === 1){//right
      return "right"
    }else if(tab[1] === 1){//bottom
      return "bottom"
    }else if(tab[0] === -1){//left
      return "left"
    }
}
function index(i, j){
    if(i < 0 || j < 0 || i > cols - 1 || j > rows - 1){
      return (-1);
    }
    return i + j * cols;
  }

function Cell(i, j) {
  this.i = i;
  this.j = j;
  this.visited = false;
  this.current = false;
  //does the wall exists? top right bottom left
  this.walls = [true, true, true, true];
  this.unvisitedNeighbours = [];
  this.show = function(){
    var x = this.i * w;
    var y = this.j * w;
    stroke(255);
    // noFill();
    // rect(x,y,w,w);
    if(this.walls[0]){
      line(x,y,x+w,y)
    }
    if(this.walls[1]){
      line(x+w,y,x+w,y+w)
    }
    if(this.walls[2]){
      line(x+w,y+w,x,y+w)
    }
    if(this.walls[3]){
      line(x,y+w,x,y)
    }
    if(this.visited){
      noStroke();
      if(this.current){
        fill(0, 255, 0)
      }else{
        fill(255, 0, 255, 100);
      }
      rect(x, y, w, w);
    }
  }
  this.removeWalls = function(which){
    if(which[1] === -1){//top
      this.walls[0] = false;
    }else if(which[0] === 1){//right
      this.walls[1] = false;
    }else if(which[1] === 1){//bottom
      this.walls[2] = false;
    }else if(which[0] === -1){//left
      this.walls[3] = false;
    }
  }
  this.checkNeighbours = function() {
    var unvisitedNeighbours = [];
        
    var neighbours = []; // top, right, bottom, left
    neighbours[0] = grid[index(this.i, this.j - 1)]; 
    neighbours[1] = grid[index(this.i + 1, this.j)]; 
    neighbours[2] = grid[index(this.i, this.j + 1)]; 
    neighbours[3] = grid[index(this.i - 1, this.j)];

    for(var i in neighbours){
      if(neighbours[i] && !neighbours[i].visited){
        unvisitedNeighbours.push(neighbours[i]);
      }
    }
    
    this.unvisitedNeighbours = unvisitedNeighbours;
    if(unvisitedNeighbours.length > 0){
      return unvisitedNeighbours[Math.floor(Math.random()*unvisitedNeighbours.length)];
    }else{
      return undefined;
    }
  }
}